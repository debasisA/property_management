<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



// Routes for user

Route::resource('user-checklist','usercontrollers\ChecklistController');
Route::get('/profile','usercontrollers\ProfileController@index')->name('user-profile');
Route::post('/profile/save','usercontrollers\ProfileController@updateProfile');
Route::get('/change-password','usercontrollers\ProfileController@resetPassword')->name('reset-password');
Route::post('/update-password','usercontrollers\ProfileController@updatePass');


// Routes for Admin.
Route::get('admin/', 'HomeController@admin')->middleware('admin');


Route::group(['prefix' => 'admin', 'namespace' => 'admincontrollers'], function () {

Route::resource('userrole','RoleController');
Route::resource('users','UserController');
Route::resource('setrole','SetroleController');
Route::resource('checklist','ChecklistController');
Route::get('checkliststatus','ChecklistController@showAllChecklist');
Route::get('checklistdata','ajaxController@index');
Route::get('checklistcreate','ajaxController@showChecklistForm');
Route::post('checklistadd','ajaxController@checklistAdd');
Route::get('checklistedit','ajaxController@checklisteditForm');
Route::post('checklistupdate','ajaxController@checklistUpdate');
Route::post('checklistdelete','ajaxController@checklistDelete');
Route::post('checkliststatus','ajaxController@checklistStatusUpdate');
Route::get('setrole/{userId}/role','SetroleController@index');

});
