<?php

namespace App\Http\Controllers\usercontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use App\User;

class ProfileController extends Controller
{
    
    # Show User Profile
    public function index(){
        $userId    =   Auth::user()->id;
    try{
        $user      =  user::find($userId);
        return view('userview/my-profile')->with('user',$user);
    }catch(Exception $ex){

          Log::error($ex);


    }
}

    # Update User profile.
    public function updateProfile(Request $request){

      $userId     =  $request->input('user_id');
      $name       =  $request->input('ProfileUserName');
      $address    =  $request->input('address');
      $phone      =  $request->input('phone');
  try{
      User::where('id',$userId)->update(['name'=>$name,'address'=>$address,'phone'=>$phone]);
      
      return redirect()->route('user-profile')->with('message','Profile Updated Successfully!!!');exit;

  }catch(Exception $ex){

     Log::error($ex);
       
    }
}

    #To Show the User Password Reset Form

    public function resetPassword(){
 
    try{
	      $userId=Auth::user()->id;
	      $user     =  user::find($userId);
	      return view('userview/change-password')->with('user',$user);

     }catch(Exception $ex){

     	 Log::error($ex);

     }
    }

    # Update Password
    public function updatePass(Request $request){

       $userId           =  $request->input('user_id');
       $givenNewPassword =  $request->input('newpassword');
       $genNewPassword   =  Hash::make($givenNewPassword);

      try{
         User::where('id',$userId)->update(['password'=>$genNewPassword]);
         return redirect()->route('reset-password')->with('message','Password has been Changed Successfully!!!');exit;
      }catch(Exception $ex){
   
       Log::error($ex);
       
      }
    }
}
