<?php

namespace App\Http\Controllers\admincontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
        
        $user         =    User::where('type',2)->paginate(10);
        return view('admin.usersdetails')->with('userdata',$user);exit;

        }catch(Exception $ex){

            Log::error($ex);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.adduser');exit;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
        $userObject           =  new User;
        $email_id             =  $request->input('email_id');
        $pass                 =  $request->input('pass');
        $genPass              =  Hash::make($pass);
        $userObject->name     =  $request->input('user_name');
        $userObject->address  =  $request->input('address');
        $userObject->phone    =  $request->input('phone');
        $userObject->email    =  $email_id;
        $userObject->password =  $genPass;
        $userObject->type     =  '2';

        $countRec=$userObject->where(['email'=>$email_id ])->get()->count();

        if($countRec > 0){
           
           return view('admin.adduser')
           ->with('message',"Email has Been Used By Other User!!!");exit;
        }else{

        $userObject->save();
        $user         =    User::where('type',2)->paginate(10);

        return view('admin.usersdetails')->with('userdata',$user)
        ->with('message',"User Added Successfully!!!");exit;
        }

      }catch(Exception $ex){

        Log::error($ex);
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
        $userData    =    User::find($id);

        return view('admin.edituser')->with('userData',$userData);exit;
     }catch(Exception $ex){

        Log::error($ex);
     }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
        $userData=User::find($id);

        $previousPass=$userData->password;
        
        $pass                 =  $request->input('pass');
        $genPass              =  Hash::make($pass);
        $name                 =  $request->input('user_name');
        $address              =  $request->input('address');
        $phone                =  $request->input('phone');

        if($pass !=""){
            $password         =  $genPass;
        }else{
            $password         =  $previousPass;
        }

        User::where('id',$id)->update(['name'=>$name,'address'=>$address,'phone'=>$phone,'password'=>$password]);


         $user         =    User::where('type',2)->paginate(10);
        return view('admin.usersdetails')->with('userdata',$user)
        ->with('message',"User Updated Successfully!!!");exit;
    }catch(Exception $ex){

        Log::error($ex);
    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            
            $user_type=User::select('type')->where('id', $id)->get();
            if($user_type[0]['type']===2){

               User::find($id)->delete();
               $user         =    User::where('type',2)->paginate(10);
              return view('admin.usersdetails')->with('userdata',$user)
              ->with('message',"User Deleted Successfully!!!");exit;
            }else{

            $user         =    User::where('type',2)->paginate(10);
            return view('admin.usersdetails')->with('userdata',$user)
            ->with('message',"Admin user can't be deleted!!!");exit;

            }
        }catch(Exception $ex){

              Log::error($ex);
        }
    }

    
}
