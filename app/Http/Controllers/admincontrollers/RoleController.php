<?php

namespace App\Http\Controllers\admincontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Log;
use App\role; 

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
         $designation   =   role::paginate(10);
        
          return view('admin.usersrole')->with('designation',$designation);
        
       }catch(Exception $ex){

        Log::eroor($ex);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view("admin.manage-role");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{

        $roleObject             =    new Role;
        $roles                  =    $request->input('role_name');
        $roleObject->role_name  =    $roles;
        
        $countRec=$roleObject->where(['role_name'=>$roles])->get()->count();

        if($countRec > 0){
         
                $designation    =    $roleObject->paginate(10);

                return view('admin.usersrole')
                ->with('designation',$designation)
                ->with('message','Role already Exist!!!');

        }else{

            $roleObject->save();
            $designation        =    $roleObject->paginate(10);

            return view('admin.usersrole')
            ->with('designation',$designation)
            ->with('message','Role Created Successfully!!!');

        }

       
        }catch(Exception $ex){

            Log::error($ex);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
         role::find($id)->delete();
         $designation    =   role::paginate(10);
        
          return view('admin.usersrole')
          ->with('designation',$designation)
          ->with('message','Role Deleted Successfully!!!');

     }catch(Exception $ex){

       Log::error($ex);
     }
    }

   
}
