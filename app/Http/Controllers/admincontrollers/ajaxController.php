<?php

namespace App\Http\Controllers\admincontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Exception;
use Illuminate\Support\Facades\Log;
use App\checklist;
use App\role;

class ajaxController extends Controller
{
	# Returning Added Checklist present in DB
    public function index(){

      try{
	      $resArray       = checklist::all();
	      $contentString  = array();
     foreach($resArray as $resChecklist)
     { 
        $subArray=array();
        
        $eventdt            =               date("D M d Y ",strtotime($resChecklist->checklist_date));
        $string             =               $resChecklist->checklist_title."\n".addslashes($resChecklist->checklist_time);
        $tool_tip_string    =               "CheckList Title : ".$resChecklist->checklist_title."\n Details:".addslashes($resChecklist->checklist_details)."\n Time : ".addslashes($resChecklist->checklist_time);
        $subArray['eid']    =               $resChecklist->id;
        $subArray['title']  =               $string;
        $subArray['start']  =               $eventdt;
        $subArray['tip']    =               $tool_tip_string;
        $subArray['allDay'] =               'false';
        array_push($contentString,$subArray);
    }

        
    	 return Response()->json($contentString);exit;

    }catch(Exception $ex){

    	Log::error($ex);
    }
    }

   # Showing Checklist Form 
   public function showChecklistForm(Request $request){

       try{
   	      $current_date    =   $request->input('cur_date');
   	      $roles           =   Role::all();

          return view('admin.addchecklist')
          ->with('checklist_date',$current_date)
          ->with('roles',$roles);exit;

        }catch(Exception $ee){

        	Log::error($ex);
        }  
   }

  # Saving Checklist Data.
   public function checklistAdd(Request $request){
     try{
     	$checklist                       =  new checklist;
        $checklist->role_id            =  $request->role;
        $checklist->checklist_title    =  $request->title;
        $checklist->checklist_details  =  $request->detail;
   	    $checklist->checklist_date     =  $request->date;
   	    $checklist->checklist_time     =  $request->time;

   	    $checklist->save();

   	    
      }catch(Exception $ex){
        
      	Log::error($ex);
      }

    }

    # Checklist Edit Form

    public function checklisteditForm(Request $request){
     
     try{

     	$eventId       =   $request->eventId;
     	$editChecklist =   checklist::find($eventId);
     	$user_roles    =   role::all();



     	return view('admin.editchecklist')
     	->with('editChecklist',$editChecklist)
     	->with('roles',$user_roles);exit;

     }catch(Exception $ex){

     	Log:error($ex);
     }

    }

   #update checklist
    public function checklistUpdate(Request $request){

            
    	try{
            
        $checklist_id      =      $request->checklist_id;
    		$role_id           =      $request->role;
    		$checklist_title   =      $request->title;
    		$checklist_details =      $request->detail;
    		$checklist_date    =      $request->date;
    		$checklist_time    =      $request->time;

    		checklist::where('id', $checklist_id)->update(['role_id'=>$role_id,'checklist_title'=>$checklist_title,'checklist_details'=>$checklist_details,'checklist_date'=>$checklist_date,'checklist_time'=>$checklist_time]);

    	}catch(Exception $ex){

    		Log::error($ex);
    	}
    }

    #Delete Checklist.
    public function checklistDelete(Request $request){
    
    try{
       $checklist_id      =      $request->checklist_id;
       checklist::where('id',$checklist_id)->delete();
     }catch(Exception $ex){

     	Log::error($ex);
     }
    }


    #Checklist Status Update Function.
    public function checklistStatusUpdate(Request $request){
    
    try{
        $checklist_id      =  $request->chklist_id;
        $status            =  $request->status; 
       checklist::where('id',$checklist_id)->update(['status' => $status]);
       
     }catch(Exception $ex){

      Log::error($ex);

      
     }
    }



}
