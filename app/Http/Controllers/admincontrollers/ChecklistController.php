<?php

namespace App\Http\Controllers\admincontrollers;

use Illuminate\Http\Request;
use Response;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Log;
use App\checklist;
use App\role;


class ChecklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.checklist');exit;
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
        $checklistId  = $request->input('del_id');


        $status=checklist::where('id',$checklistId)->select('status')->get();

        if($status[0]->status ===0 || $status[0]->status ===2)

        {
            checklist::where('id',$checklistId)->delete();
            $checlists=checklist::paginate(10);
            return view('admin.checkliststatus')
             ->with('checlists',$checlists)
            ->with('message',"Checklist Delete Successfully!!!");exit;
        }else{

            $checlists=checklist::paginate(10);
            return view('admin.checkliststatus')
            ->with('checlists',$checlists)
            ->with('message',"Work Started , Checklist Cant't Be Deleted!!!");exit;
        }

       }catch(Exception $ex){

          Log::error($ex);
       }

    }

   
    #Getting All checklist.
    public function showAllChecklist(){

     try{

        $checlists=checklist::paginate(10);
        return view('admin.checkliststatus')->with('checlists',$checlists);exit;

      }catch(Exception $ex){

        Log::error($ex);

      }
      
        
    }

    
}
