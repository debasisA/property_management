<?php

namespace App\Http\Controllers\admincontrollers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\Log;
use App\role;
use App\User;
use App\userrole;


class SetroleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)

    {
       try{ 
       $users   =  User::find($id);
       $roles   =  role::all();
       
       
     $permi=userrole::where('user_id',$id)->get();
     
     $all_data  =  array();
     if($permi->isNotEmpty()){
     foreach($permi as $perm){
        $all_data[]=$perm->role_id;
     }
    }else{

       $all_data[]  =  0;
   }

       return view('admin.adduser-role')
       ->with('userdata',$users) 
       ->with('rolesdata',$roles)
       ->with('permission',$all_data);exit;  

      }catch(Exception $ex){

         Log::error($ex);
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId         =   $request->input('user_id');

        try{

        $userRoleObject =   new userrole;

        
        $countRec       =   $userRoleObject->where(['user_id'=>$userId])->get()->count();

        if($countRec >0){

            $userRoleObject->where('user_id',$userId)->delete();

        }

        $userRoles      =    $request->input("userRole");
       
        $allRoles       =    [];
        foreach($userRoles as $val){
      
                 $userRoleObject->user_id   =    $userId;
                 $userRoleObject->role_id   =    $val;
                 $allRoles[]                =    $userRoleObject->attributesToArray();

        }
        $userRoleObject->insert($allRoles);

        return redirect()->back();

        }catch(Exception $ex){


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
