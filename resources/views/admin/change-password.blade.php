<?php

	ob_start();
	session_start();

	# Checck the Logged In session
	if(!isset($_SESSION['admin_id'])){	
		header("Location:index");
		exit;
	}
	include "../includes/class.Main.php";


	$mainClassObject=new Main();  
	  
	include "../appsTop.php";

	

	#User Password update.
	if(isset($_REQUEST['operation']) && $_REQUEST['operation']=="edit"){
		
		$oldPassword = base64_encode(base64_encode($_POST["oldpassword"]));	
		$newPassword = base64_encode(base64_encode($_POST["newpassword"]));
		# Admin Password Update
		if($_SESSION['admin_id'] !=""){
			$countRecord = $mainClassObject->countRows('core',"password='$oldPassword' AND id = '$_SESSION[admin_id]'");
			if($countRecord > 0){
				$passString = "password = '$newPassword'";
				$mainClassObject->updateTable("core",$passString," id = '$_SESSION[admin_id]'");
				
				header("Location:change_password?msg=success");
				exit;
			}else{
				header("Location:change_password?msg=invalid");
				exit;
			}
		}
	} 
?>


<?php
  
  #---- Header Includes Part--- #
  include "../header.php";
  include "left_menu.php";

?>
    <section class="main_content clearfx">
    	
        <div class="container-fluid">
            <div class="row">
            	<div class="col-md-12">
                    <div class="panel panel-default row">
                    	<h4 class="mar_left_15">Change Password.</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
              <div class="panel panel-success">
                <div class="panel-heading">Change Password</div>
                <div style="color:#900; text-align:center"><?php if(isset($_REQUEST['msg']) && $_REQUEST['msg']=='success'){ echo "Password Changed Successfully!!!"; } ?></div>
                 <div style="color:#900; text-align:center"><?php if(isset($_REQUEST['msg']) && $_REQUEST['msg']=='invalid'){ echo "Mismetched Old Password!!!"; } ?></div>
                <div class="clear"></div>
                <div class="panel-body">
                   <form role="form" name="change_password" action="" method="post" onSubmit="return validatePassword();">                   
                   <input class="form-control" type="hidden" name="operation" id="operation" value="edit"/>                   
                     <div class="form-group col-md-12">
                        <label class="control-label" for="">Old Password</label>
                        <input class="form-control" id="oldpassword" name="oldpassword"  type="password" >
                      </div>
                      <div class="form-group col-md-12">
                        <label class="control-label" for="">New Password</label>
                        <input class="form-control" id="newpassword" name="newpassword"  type="password" >
                      </div>
                      <div class="form-group col-md-12">
                        <label class="control-label" for="">Confirm Password</label>
                        <input class="form-control" id="confirm_password" name="confirm_password"  type="password" >
                      </div>
                        </br>                    
                      <div class="form-group col-md-12">
                      <button type="submit" class="btn btn-danger">&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;</button>
                      </div>
                     
                  </form>
             </div>
                </div>
              </div>
            </div>
        </div>
    </div>
        
    </section>
    
 <?php include "../footer.php"; ?>

<script>
function validatePassword(){
	if(	$('#oldpassword').val()==""){
		$('#oldpassword').focus();
		return false;
	}else if($('#newpassword').val()==""){
		$('#newpassword').focus();
		return false;
	}else if($('#confirm_password').val()==""){
		$('#confirm_password').focus();
		return false;
	}else if($('#newpassword').val()!=$('#confirm_password').val()){
		$('#confirm_password').focus();
		return false;
	}
}
</script>