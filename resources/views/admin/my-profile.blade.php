<?php
	ob_start();
	session_start();
	include "../includes/class.Main.php";

    # Checking Session.	
	if(!isset($_SESSION['admin_id'])){	
	header("Location:index.php");
	exit;
	}
	# Object creation of the Main class.
	$mainClassObject=new Main();   

	include "../appsTop.php";


	# Checking User Logged in Status
	if(!isset($_SESSION['admin_id'])){	
		header("Location:index");
		exit;
	}

	# Getting Data for Editing based on the User Login Type.
	

	if($_SESSION['admin_id'] != ""){
		
		$resProfile       =    $mainClassObject->fetchSingle("core","*","id='$_SESSION[admin_id]'");
		$name             =    $resProfile ['admin_name'];
	}


	# Profile Update Operation taking place here.

	if($_REQUEST['update_operation']=="update")
	{
		
	if($_SESSION['admin_id'] != "" ){
		
	$string         =      "admin_name='$_REQUEST[ProfileUserName]', email='$_REQUEST[ProfileEmailID]',alt_email='$_REQUEST[ProfileAltEmailID]',site_url='$_REQUEST[SiteUrl]',powered_by='$_REQUEST[powered_by]'";

	$mainClassObject->updateTable("core",$string,"id='$_SESSION[admin_id]'");

	}
	
	header("Location:my_profile?msg=001");
	}
?>
<script>
    // Data Validation
	function validateProfileData(){		
		if($("#ProfileUserName").val()==""){
			$("#ProfileUserName").focus();
			return false;
		}
		if($("#ProfileEmailID").val()==""){
			$("#ProfileEmailID").focus();
			return false;
		}
		
		if($("#ProfileAltEmailID").val()==""){
			$("#ProfileAltEmailID").focus();
			return false;
		}
		
		if($("#SiteUrl").val()==""){
			$("#SiteUrl").focus();
			return false;
		}
		
		if($("#powered_by").val()==""){
			$("#powered_by").focus();
			return false;
		}
		
		
		
	}

 // Number Validation.	
 function checkNums(id)
 {
	var x=$('#'+id).val().replace(/[^0-9\s-.]/g,'');
	 $('#'+id).val(x);
   }
</script>

<?php
  
  #---- Header Includes Part--- #
  include "../header.php";
  include "left_menu.php";
?>	
     
    <section class="main_content">
    	
        <div class="container-fluid">
            	<div class="row">
            	<div class="col-md-12">
                    <div class="panel panel-default row">
                    	<h4 class="mar_left_15">[MY PROFILE]</h4>
                    </div>
                </div>
              	</div>
            
                <div class="col-md-12">
                <div class="panel-body">
                <?php if(isset($_REQUEST['msg']) && $_REQUEST['msg']==001){?> 
				
                <div style="color:#900; text-align:center">Profile Updated Successfuly!!!</div>
                <?php }?>
				
                    <form role="form" name="profile" action="" method="post" onSubmit="return validateProfileData();" >
                    
                    <input class="form-control" type="hidden" name="update_operation" id="update_operation" value="update"/>
                    
                    <div class="form-group col-md-4">
                    <label class="control-label" for="user_name">Name</label>
                    <input class="form-control" id="ProfileUserName" name="ProfileUserName"  type="text" value="<?php echo $name;?>"  autocomplete="off" required >
                    </div>
					<div class="clear"></div>
					
                   
					
					 <div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="user_name">Email ID</label>
                    <input class="form-control" id="ProfileEmailID" name="ProfileEmailID"  type="text" value="<?php echo $resProfile['email'];?>" >
                    </div>
					
					<div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="user_name">Alt Email ID</label>
                    <input class="form-control" id="ProfileAltEmailID" name="ProfileAltEmailID"  type="text" value="<?php echo $resProfile['alt_email'];?>" required autocomplete="off">
                    </div>
                    <div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="login_id">Site URL</label>
                    <input type="text" name="SiteUrl" id="SiteUrl" class="form-control" value="<?php echo $resProfile['site_url'];?>">
                    
                    </div>
                    
                    <div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="login_id">Powered By</label>
                    <input class="form-control" id="powered_by" name="powered_by" type="text" value="<?php echo $resProfile['powered_by']; ?>" required autocomplete="off"/>
                    </div>
                    
                    <div class="clear"></div>
                    <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-danger">Save</button>
					
                    <button type="button" class="btn btn-success" onclick="javascript:window.location.href='admin_dashboard.php'">Close</button>
                    </div>
                    </form>
            
                </div>
                </div>
        </div>
    </div>
        
    </section>
    
 <?php include "../footer.php"; ?>
