@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu')  
     


    <section class="main_content">
    	
        <div class="container-fluid">
            <div class="row">
            	<div class="gray_cont_box clearfix">
            	<h4 class="col-lg-12">Set User Role</h4>
            </div>
            <div class="col-md-12">
            		<div class="col-md-6">
              <div class="panel panel-success">
                <div class="panel-heading" style="font-size:18px; font-weight:bold;">Select Roles For {{$userdata->name}}</div>
                <div class="clear"></div>
                <div class="panel-body">
                    <form name="frmRole" id="frmRole" method="post" onSubmit="return validate()" action="{{url('admin/setrole')}}" >
                    	@csrf
                    <input type="hidden" name="action" value="role">
                    <input type="hidden" name="user_id" value="{{$userdata->id}}">
					
                	<table  border="0" cellspacing="0" cellpadding="0" style="margin-left:50px;">
                                
                    <tr><td>&nbsp;</td></tr>
					           @foreach($rolesdata as  $res_roles)
                                     
                                  <tr>
                                    <td><input type="checkbox" name="userRole[]" id="userRole{{$res_roles->id}}" value="{{$res_roles->id}}"   onClick="validateRole(this.value)"  

                                       @if(isset($permission)) {{ in_array($res_roles->id,$permission)? 'checked':'' }}

                                       @endif

                                    />

                                    </td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td>{{$res_roles->role_name}}</td>
                                    <td><input type="hidden" name="user_role[]" id="user_role{{$res_roles->id}}" value=
     
                                      @if(isset($permission)) {{ in_array($res_roles->id,$permission)? $res_roles->id:0 }}

                                       @endif
                                      />
                                    </td>
                                  </tr>
                                  <tr><td>&nbsp;</td></tr>
                      @endforeach
                                 
                    </table>
                    <div class="dropdown pull-right">
                    	<input type="submit" class="btn btn-primary" value="SET ROLES" />
                        <input type="button" class="btn btn-primary" value="BACK" onClick="window.location='{{url('admin/users')}}'" />
                    </div>
                            
                    </form>    
                   </div>
                </div>
              </div>
		  </div>
	</div>
</div>
</div>
</section>
    
@include('layouts.footer') 
 

 <script type="text/javascript">
 
 // Validate Whether Roles has been Selected or Not.
 function validate(){
	 
	var roles = $("input[name='userRole[]']").serializeArray(); 
	if (roles.length === 0) 
	{ 
	alert('Select Role for the User'); 
	return false;
	} 
	
	if(($("#user_role5").val()>0) && ($("#user_role7").val()>0)){
		alert("Property Manager cant be a  Renter");
		$("#user_role5").val(0);
		$("#user_role7").val(0);
		$('#userRole5').attr('checked',false);
		$('#userRole7').attr('checked',false);
		return false;
	}
	 
 }

function validateRole(role){
  
	if(role==5){
	$('#userRole5').change(function() {
        if(this.checked) {

        	$("#user_role"+role).val(role);
    }else{
    	  $("#user_role"+role).val(0);
    }
});
}
	
  if(role==7){
	$('#userRole7').change(function() {
        if(this.checked) {

        	$("#user_role"+role).val(role);
    }else{
    	$("#user_role"+role).val(0);
    }
});
}
}



</script>