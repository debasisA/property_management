@include('layouts.appstop')

<div id="myModal_edit" class="modal fade" role="dialog">
  <div class="modal-dialog">
   <div id="addchecklist_msg" style="display:none;"></div>
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Checklist</h4>
      </div>
      <div class="modal-body">
	  <form name="frmCheckList" id="frmCheckList" method="post"  autocomplete="off" >
        @csrf
        <div class="form_top clearfx">
            <span class="title"></span>
            
			<input type="hidden" name="checklist_id" id="checklist_id" value="{{$editChecklist->id}}"/>
            <input type="hidden" class="form-control" id="checklist_date" name="checklist_date" value="{{ date('Y-m-d',strtotime($editChecklist->checklist_date))}}" />
            <hr />
        </div>
        <div class="div-table">
            <div class="div-tr">
                <div class="div-th">
                    <label for="date">Date <span class="text-danger"></span>:</label>
					{{ date('d M, y',strtotime($editChecklist->checklist_date))}}
                </div>
            </div>
            <div class="div-tr">
                <div class="div-th">
                    <label for="role">Role <span class="text-danger"> *</span> :</label>
                </div>
                <div class="div-td">
                    <select class="form-control select2" name="role" id="role">
                        <option value="">-- Select Role--</option>
                        @foreach($roles as $user_roles)
                        <option value="{{$user_roles->id}}" 

                            @if($user_roles->id == $editChecklist->role_id ){{ "selected"}} @endif 
                            >
                            {{ $user_roles->role_name}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="div-tr">
                <div class="div-th">
                    <label for="time">Time <span class="text-danger"> *</span> :</label>
                </div>
                <div class="div-td">
                    <div class="bootstrap-timepicker" id="div_event_time">
                        <input type="text" class="form-control" id="checklist_time" name="checklist_time" value="{{  $editChecklist->checklist_time }}"/>
                    </div>
                </div>
            </div>
            <div class="div-tr">
                <div class="div-th">
                    <label for="checklist_title">Title <span class="text-danger"> *</span> :</label>
                </div>
                <div class="div-td">
                    <input type="text" class="form-control" id="checklist_title" name="checklist_title" autocomplete="off" value="{{ $editChecklist->checklist_title }}">
                </div>
            </div>
            
            <div class="div-tr">
                <div class="div-th">
                    <label for="checklist_details"> Details<span class="text-danger"> *</span> :</label>
                </div>
                <div class="div-td">
                    <textarea class="form-control" rows="4" placeholder="Checklist Details" name="checklist_details" id="checklist_details">{{ $editChecklist->checklist_details}}</textarea>
                </div>
            </div>
            <div class="div-tr">
                <div class="div-td"></div>
                <div class="div-td">
                </div>
            </div>
        </div>
    </form>
      </div>
      <div class="modal-footer">
	     <button class="btn btn-primary" type="button"  onclick="submitUpdateChecklist()">Update CheckList</button>
		 
		 <button class="btn btn-primary" type="button"  onclick="deleteChecklist()">Delete Checklist</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">

     var config = {
        '.chosen-select': {
            width: "100%"
        }
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }


 //TIMEPICKER Initialisation
$("#checklist_time").timepicker({
    showInputs: false
});

    // Data Validation Function.
    function validateUpadateChecklist() 
    {
        if ($('#role').val() == "") 
        {
            $("#role").focus();
            return false;
        } else if ($('#checklist_time').val() == "") {
            $("#checklist_time").focus();
            return false;
        } else if ($("#checklist_title").val() == "") {
            $("#checklist_title").focus();
            return false;
        } else if ($('#checklist_details').val() == "") {
            $("#checklist_details").focus();
            return false;
        } else {
            return true;
        }
    }

    // Data Updation through Ajax Call.
    function submitUpdateChecklist() 
    {
        var result = validateUpadateChecklist();
        if (result) {
        var token   = '{{csrf_token()}}';
        var id      = $("#checklist_id").val();
        var role    = $("#role").val();
        var date    = $("#checklist_date").val();
        var time    = $("#checklist_time").val();
        var title   = $("#checklist_title").val();
        var detail  = $("#checklist_details").val();

         $.ajax({
            async: false,
            url: '{{url("admin/checklistupdate")}}',
            type: 'POST',
            data: {'checklist_id':id,'role':role,'date': date,'time': time,'title': title,'detail': detail,'_token':token},
                success: function (res) {
                    
                   
                var msg        = "CheckList Updated Successfully";
                
                alert(msg);
                $("#myModal").modal("hide");
                window.location = "{{ url('admin/checklist') }}";
                },


            error: function (err) {
                var msg= "CheckList not Updated!!!";
                alert(msg);
                console.log(err);
            },
        });
      
        }
    }
    
    // Data Deletion through Ajax Call.
    function deleteChecklist(){
        
        var ask=confirm("Are you Want to Delete this CheckList");
        if (ask) {
        var token   = '{{csrf_token()}}';
        var id      = $("#checklist_id").val();
        

         $.ajax({
            async: false,
            url: '{{url("admin/checklistdelete")}}',
            type: 'POST',
            data: {'checklist_id':id,'_token':token},
                success: function (res) {
                   
                   
                var msg        = "CheckList Deleted Successfully";
                
                alert(msg);
                $("#myModal").modal("hide");
                window.location = "{{ url('admin/checklist') }}";
                },


            error: function (err) {
                var msg= "CheckList not Deleted!!!";
                alert(msg);
                console.log(err);
            },
        });
      
        }
            
        
    }
</script> 