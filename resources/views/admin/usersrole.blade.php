@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu') 
 
    <section class="main_content">
    	
        <div class="container-fluid">
            <div class="row">
            	<div class="gray_cont_box clearfix">
            	<h4 class="col-lg-12">User Roles
                	<div class="dropdown pull-right">
                    	
                       <a href="{{ url('admin/userrole/create')}}" class="btn btn-primary" type="button"  >Add Role</a>
                      
                    </div>
                </h4>
                
            </div>
            
                   @if(isset($message))
                    <div style="color:#900; text-align:center"> {{ $message }}</div>

                   @endif
                    <div class="col-md-12">
                    <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                            <th class="text-center">User Role</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                          @foreach($designation as $role)					     
                           <tr>
                             <td align="center">{{$role->role_name}}</td>
                             <td align="center">
                             <a href="javascript:confirm('Are you sure want to Delete this Role?'');"><img src="{{asset('images/cancel1.png')}}" alt="Delete" onclick="del_record('{{$role->id}}')" title="Delete Type" /></a>&nbsp;
                             

                             <form role="form" name="del_form{{$role->id}}"  id="del_form{{$role->id}}" method="post" action="{{url('admin/userrole')}}/{{$role->id}}">
                      
                            <input type="hidden" name="del_id" id="del_id" value="{{$role->id}}"/>
                            {{ method_field('DELETE') }}
                            @csrf

                           </form>
                           </td>
                          </tr>

                        
                         @endforeach
                          
                        </tbody>
                      </table>
                      </div>
                       <div  align="center">{{$designation->links()}}</div>
					           
					
					
              	</div>
            </div>
         </div>
    </section>
   
 @include('layouts.footer')
 
<script>
 
  

	// Function to Delete User's Role.
	function del_record(id){
   
		
		var ask=confirm("Are you sure want to Delete this Role?");
	  if(ask){
		$('#del_form'+id).submit();
	}
	}
   
 </script>