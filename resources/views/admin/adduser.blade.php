@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu') 
     
	<section class="main_content">
        <div class="container-fluid">
            	<div class="row">
            	<div class="col-md-12">
                    <div class="panel panel-default row">
                    	<h4 class="mar_left_15">Add User</h4>
                    </div>
                </div>
              	</div>
				
                @if(isset($message))
                <div style="color:#900; text-align:center">{{ $message }}</div>
				@endif
				<div style="color:#900; text-align:center"></div>
				
				
				
                <div class="col-md-12">
                <div class="panel-body">
                    <form role="form" name="users" action="{{url('admin/users')}}" method="post" onSubmit="return validateData();">
                    	<input type="hidden" id="edit_id" name="edit_id" value=""/>
                    @csrf
					
					
                    <div class="form-group col-md-4">
                    <label class="control-label" for="user_name">Name</label>
                    <input class="form-control" id="user_name" name="user_name"  type="text" value="" required>
                    </div>
					
					<div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="address">Address</label>
                    
					<textarea class="form-control" name="address" id="address" ></textarea>
                    </div>
					
					<div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="phone">Phone</label>
                    <input class="form-control" id="phone" name="phone" type="text" value="" onkeyup="checkNums(this.id)" / >
                    </div>
                    <div class="clear"></div>
					
                    <div class="form-group col-md-4">
                    <label class="control-label" for="email">Email</label>
                    <input class="form-control" id="email_id" name="email_id" type="email" value="" />
                    </div>
                    
                    <div class="clear"></div>
					
                    <div class="form-group col-md-4">
                    <label class="control-label" for="password">Password</label>
                    <input class="form-control" id="pass" name="pass" type="password"/>
                    </div>
                    
                    
                    
                    <div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="login_id">Confirm Password</label>
                    <input class="form-control" id="pass_re" name="pass_re" type="password" />
                    </div>
                   
                    
                    <div class="clear"></div>
                    <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-danger">Save</button>
                    <button type="button" class="btn btn-success" onclick="">Close</button>
                    </div>
                    </form>
            
                </div>
                </div>
        </div>
    </div>
        
    </section>
    @include('layouts.footer')



<script>

	// Data Validation function
	function validateData(){
		if($("#user_name").val()=="")
		{
			$("#user_name").focus();
			return false;
		}
		if($("#address").val()=="")
		{
			$("#address").focus();
			return false;
		}

		if($("#phone").val()=="")
		{
			$("#phone").focus();
			return false;
		}

		if($("#email_id").val()=="")
		{
			$("#email_id").focus();
			return false;
		}
		
		if($("#pass").val()==""){
			$("#pass").focus();
			return false;
		}	

		if($("#pass_re").val()==""){
			$("#pass_re").focus();
			return false;
		}	

		if($("#pass").val() !== $("#pass_re").val()){

			alert('Password Mismatch');
			return false;
		}			
		

	}

	function checkNums(id){
	var x=$('#'+id).val().replace(/[^0-9\s-.]/g,'');
	$('#'+id).val(x);
	}
</script>