@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu') 
    <section class="main_content">
    	
        <div class="container-fluid">
            <div class="row">
            	<div class="gray_cont_box clearfix">
            	<h4 class="col-lg-12">User's List
                	<div class="dropdown pull-right">
                    	
                        <a href="{{ url('admin/users/create')}}" class="btn btn-primary" type="button"  >Add User</a>
                    </div>
                </h4>
                
            </div>
                   @if(isset($message))
                    <div style="color:#900; text-align:center">{{ $message }}</div>

                    @endif
                    <div class="col-md-12">
                    <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                            <th style="width:10%; text-align:center;">Name</th>
                            <th style="width:30%; text-align:center;">Address</th>
                            <th style="width:10%; text-align:center;">Phone</th>
							               <th style="width:10%; text-align:center;">Email</th>
							             <th style="width:10%; text-align:center;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
						            
                        @if(isset($userdata))
                         @foreach($userdata as $user)
                          <tr>
                             <td align="center">{{$user->name}}</td>
              							 <td align="center">{{$user->address}}</td>
                             <td align="center">{{$user->phone}}</td>
              							 <td align="center">{{$user->email}}</td>
                             
                             <td align="center">
                             
                             <a href="{{url('admin/users')}}/{{($user->id)}}/{{'edit'}}"><img src="{{ asset('images/edit.png') }}" alt="Edit" title="Edit User" /></a>&nbsp;
                              
                              <a href="{{url('admin/setrole')}}/{{($user->id)}}/{{'role'}}"><img src="{{ asset('images/setpermission.png') }}" alt="Edit" title="SET ROLES" /></a>&nbsp;
                            
                             
                             <a href="javascript:void(0);"><img src="{{ asset ('images/cancel1.png') }}" alt="Delete" onclick="deleteUser('{{$user->id}}')" title="Delete User" /></a>&nbsp;
                             
                             
                             </td>
                             <form role="form" name="del_form" method="post" action="{{url('admin/users')}}/{{$user->id}}">
                      {{method_field('DELETE')}}
                      @csrf
                       <input type="hidden" name="del_operation" id="del_operation" value="del"/>
                      <input type="hidden" name="del_id" id="del_id" value=""/>
                    </form>
                          </tr>
                         @endforeach
                         @endif
                        </tbody>
                      </table>
                      </div>
                    <div  align="center">{{$userdata->links()}}</div>
                      
                    
              	</div>
            </div>
         </div>
    </section>
    
@include('layouts.footer')
<script>
 
	function frmRedirect()
	{
	 window.location.href="user-management";
	}

	// Delete User.
	function deleteUser(id)
	{
	  $("#del_id").val(id);
	  var ask=confirm("Are you sure want to Delete this User?");
	  if(ask){
	  document.del_form.submit();
	}
	}
   
 </script>