@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu') 

     
    <section class="main_content">
    	
        <div class="container-fluid">
            	<div class="row">
            	<div class="col-md-12">
                    <div class="panel panel-default row">
                    	<h4 class="mar_left_15">Manage Role</h4>
                    </div>
                </div>
              	</div>
                 
                <div style="color:#900; text-align:center"></div>
                
                <div class="col-md-12">
                <div class="panel-body">
               
                    <form role="form" name="roles" action="{{url('admin/userrole')}}" method="POST" onSubmit="return validateRole();">
                    
                    @csrf   
                     
                   
                    <div class="form-group col-md-4">
                    <label class="control-label" for="role">Role</label>
                    <input class="form-control" id="role_name" name="role_name"  type="text" value="" required>
                    </div>
                    
                    
                    <div class="clear"></div>
                    <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-danger">Save</button>
                    <a href="{{url('admin/userrole')}}" class="btn btn-success" >Close</a>
                    </div>
                    </form>
            
                </div>
                </div>
        </div>
    </div>
        
    </section>
    
  
 @include('layouts.footer')

<script>
    // Data Vaidation Function.
	function validateRole(){		
		if($("#role_name").val()==""){
		$("#role_name").focus();
		return false;
		}

	}

    
</script>