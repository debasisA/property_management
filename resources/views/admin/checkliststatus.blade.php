@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu') 
 
 
    
    <section class="main_content">
    	
        <div class="container-fluid">
            <div class="row">
            	<div class="gray_cont_box clearfix">
            	<h4 class="col-lg-12">Check List Status
                	<div class="dropdown pull-right">
                      
                    </div>
                </h4>
                
            </div>
                   @if(isset($message))
                    <div style="color:#900; text-align:center">{{ $message }}</div>

                    @endif
                    <div class="col-md-12">
                    <table class="table table-hover table-bordered">
                        <thead>
                          <tr>
                            <th style="width:10%; text-align:center;">Date</th>
                            <th style="width:10%; text-align:center;">Time</th>
                            <th style="width:20%; text-align:center;">Work Title</th>
              							<th style="width:30%; text-align:center;">Details</th>
              							<th style="width:15%; text-align:center;">Role</th>
              							<th style="width:15%; text-align:center;">Status</th>
              							<th style="width:10%; text-align:center;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        	@if(isset($checlists))
                            @foreach($checlists as $checklist)
                          <tr>
                             <td align="center">{{ date('d-m-Y',strtotime($checklist->checklist_date))}}</td>
							 <td align="center"> {{ $checklist->checklist_time }}</td>
                             <td align="center">{{ $checklist->checklist_title }}</td>
							 <td align="center">{{ $checklist->checklist_details }}</td>
							 <td align="center">{{ $checklist->role_id }}</td>
							 <td align="center">@if($checklist->status==2){{"Completed"}} @elseif($checklist->status==1) {{ "Started"}} @else {{"Not Started"}} @endif </td>
                             
                             <td align="center">
                             
                             <a href="javascript:void(0);"><img src="{{asset('images/cancel1.png')}}" alt="Delete" onclick="deleteChecklist('{{$checklist->id}}')" title="Delete Checklist" /></a>&nbsp;
							  
							  
                             </td>
                          </tr>

                          @endforeach
                          @endif
						  
                        </tbody>
                      </table>

                         
                          
                      </div>
					  
					        <div  align="center">{{ $checlists->links() }}</div>

                  <form role="form" name="del_form" method="post" action="{{url('admin/checklist')}}/{{$checklist->id}}">
                      {{method_field('DELETE')}}
                      @csrf
                       
                      <input type="hidden" name="del_id" id="del_id" value=""/>
                    </form>
					   
              	</div>
            </div>
         </div>
    </section>
 @include('layouts.footer')
 
 
 <script>
	  // Delete Check List.
	function deleteChecklist(id)
  {
    $("#del_id").val(id);
    var ask=confirm("Are you sure want to Delete this User?");
    if(ask){
    document.del_form.submit();
  }
  }
   
 </script>
