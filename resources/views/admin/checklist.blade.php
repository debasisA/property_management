@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu') 

	<div id="showModel"></div>
	
    <section class="main_content">
        <div class="container-fluid">
            <div class="row">
            	<div class="gray_cont_box clearfix">
            	<h3 class="col-lg-12">Check List
                </h4>
            </div>
                    <div style="color:#900; text-align:center"></div>
                    <div class="col-md-12">
					<div id ='calendar'></div>
					
				</div>
            </div>
         </div>
    </section>
   
 @include('layouts.footer')

<script type='text/javascript'>

$(document).ready(function() 
{

  
			 
	var currentDate = new Date();  
	var d = currentDate.getDate();  
	var m = currentDate.getMonth();
	var y = currentDate.getFullYear();

	$('#calendar').fullCalendar({
		theme: true,
		header: {
			left: 'prev,next today',
			center: 'title',
			
		},
		editable: true,
				
		eventSources: [getCalData()], 
		eventRender: function eventRender( event, element, view ) {
			
			element.attr('title', event.tip);
	   },

		eventClick: function(event, jsEvent, view){ 
			editCheckList(event.eid);
		},
		
		
		dayClick: function(date, allDay, jsEvent, view) { 
		
		var eventdate = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
		var present=currentDate.getFullYear()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getDate();
		var eventMonth=	date.getMonth()+1;
		var currentMonth=currentDate.getMonth()+1;
		if((eventdate === present) || (eventdate > present)){ 
			addCheckList(eventdate);
		}
		},
		
		
});


	function getCalData() {
		
		var source = '';
		var token = '{{csrf_token()}}';
		$.ajax({
			async: false,
			url: '{{url("admin/checklistdata")}}',
			type: 'GET',
			dataType: 'json',
			data: {'choice':'allEvent','_token':token},
			success: function (data) {
				
				source=data;
			},
			error: function (err) {
				console.log(err);
			},
		});
		return source;
	}
		
		
	$('.fc-button-prev').click(function(){
		
		var view = $('#calendar').fullCalendar('getView');
		var viewname = view.name;
		var startdate = view.start.getFullYear()+"-"+(view.start.getMonth()+1)+"-"+view.start.getDate();
		var enddate = view.end.getFullYear()+"-"+(view.end.getMonth()+1)+"-"+view.end.getDate();
		$('#calendar').fullCalendar('removeEvents');//remove previous events
		$('#calendar').fullCalendar('addEventSource', getCalData(startdate,enddate));//calling the getcaldata function
		
	});
		
	$('.fc-button-next').click(function(){
		var view = $('#calendar').fullCalendar('getView');
		var viewname = view.name;
		var startdate = view.start.getFullYear()+"-"+(view.start.getMonth()+1)+"-"+view.start.getDate();
		var enddate = view.end.getFullYear()+"-"+(view.end.getMonth()+1)+"-"+view.end.getDate();
		$('#calendar').fullCalendar('removeEvents');//remove previous events
		$('#calendar').fullCalendar('addEventSource', getCalData(startdate,enddate));
	});

	//WEEK VIEW 
	$('.fc-button-agendaWeek').click(function(){
		
		var view = $('#calendar').fullCalendar('getView');
		var viewname = view.name;
		var startdate = view.start.getFullYear()+"-"+(view.start.getMonth()+1)+"-"+view.start.getDate();
		var enddate = view.end.getFullYear()+"-"+(view.end.getMonth()+1)+"-"+view.end.getDate();
		
	});
	
	//DAY VIEW 
	$('.fc-button-agendaDay').click(function(){
		
		var view = $('#calendar').fullCalendar('getView');
		var viewname = view.name;
		var startdate = view.start.getFullYear()+"-"+(view.start.getMonth()+1)+"-"+view.start.getDate();
		var enddate = view.end.getFullYear()+"-"+(view.end.getMonth()+1)+"-"+view.end.getDate();
		
	});
	});
	
	
	
	// Add Check List
	function addCheckList(date){
		
         var token = '{{csrf_token()}}';
         $.ajax({
			async: false,
			url: '{{url("admin/checklistcreate")}}',
			type: 'GET',
			data: {'cur_date': date,'_token':token},
			success: function (res) {
				$("#showModel").html(res);
		        $("#myModal").modal("show");
			},
			error: function (err) {
				console.log(err);
			},
		});
	}

	

	// Edit Check List
	function editCheckList(eventId){ 


		 var token = '{{csrf_token()}}';
         $.ajax({
			async: false,
			url: '{{url("admin/checklistedit")}}',
			type: 'GET',
			data: {'eventId': eventId,'_token':token},
			success: function (res) {
				$("#showModel").html(res);
		        $("#myModal_edit").modal("show");
			},
			error: function (err) {
				console.log(err);
			},
		});
		
		
	}
	
</script>