@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu')
     
    <section class="main_content">    	
        <div class="container-fluid">
            	<div class="row">
            	<div class="col-md-12">
                    <div class="panel panel-default row">
                    	<h4 class="mar_left_15">[MY PROFILE]</h4>
                    </div>
                </div>
              	</div>
            
                <div class="col-md-12">
                <div class="panel-body">
                @if( Session::has('message') )
                <div style="color:#900; text-align:center">{{ Session::get('message') }}</div>
                @endif 
				
                    <form role="form" name="profile" action="{{url('/profile/save')}}" method="post" onSubmit="return validateProfileData();" >
                   	@csrf
                    
                    
                     <input class="form-control" type="hidden" name="user_id" id="user_id" value="{{ $user->id }}"/>
                    
                    <div class="form-group col-md-4">
                    <label class="control-label" for="user_name">Name</label>
                    <input class="form-control" id="ProfileUserName" name="ProfileUserName"  type="text" value="{{$user->name}}"  autocomplete="off" required >
                    </div>
					
					<div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="address">Address</label>
                    
					<textarea class="form-control" name="address" id="address" required >{{ $user->address }}</textarea>
                    </div>
					
					<div class="clear"></div>
                    <div class="form-group col-md-4">
                    <label class="control-label" for="phone">Phone</label>
                    <input class="form-control" id="phone" name="phone" type="text" value="{{ $user->phone }}" onkeyup="checkNums(this.id)" required / >
                    </div>
                    
                    <div class="clear"></div>
                    <div class="form-group col-md-4">
                    <button type="submit" class="btn btn-danger">Save</button>
					<button type="button" class="btn btn-success" onclick="redirect()">Close</button>
                    </div>
                    </form>
            
                </div>
                </div>
        </div>
    </div>
        
    </section>
    
 @include('layouts.footer')
<script>
	// Data Validation
	function validateProfileData(){		
	if($("#ProfileUserName").val()==""){
		$("#ProfileUserName").focus();
		return false;
	}
	if($("#address").val()==""){
		$("#address").focus();
		return false;
	}

	if($("#phone").val()==""){
		$("#phone").focus();
		return false;
	}
	}

	// Number Validation.	
	function checkNums(id)
	{
	var x=$('#'+id).val().replace(/[^0-9\s-.]/g,'');
	$('#'+id).val(x);
	}

	function redirect(){

	window.location="{{ route('home') }}";
	}
</script>