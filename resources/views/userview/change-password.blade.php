@include('layouts.appstop')
@include('layouts.header')
@include('layouts.leftmenu')

    <section class="main_content clearfx">
    	
        <div class="container-fluid">
            <div class="row">
            	<div class="col-md-12">
                    <div class="panel panel-default row">
                    	<h4 class="mar_left_15">Change Password.</h4>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
              <div class="panel panel-success">
                <div class="panel-heading">Change Password</div>
               @if(Session::has('message'))
                <div style="color:#900; text-align:center">{{ Session::get('message') }}</div>
               @endif
                 
                <div class="clear"></div>
                <div class="panel-body">
                   <form role="form" name="change_password" action="{{ url('/update-password') }}" method="post" onSubmit="return validatePassword();">   
                   @csrf                
                   <input class="form-control" type="hidden" name="user_id" id="user_id" value="{{ $user->id }}"/>                   
                    
                      <div class="form-group col-md-12">
                        <label class="control-label" for="">New Password</label>
                        <input class="form-control" id="newpassword" name="newpassword"  type="password" >
                      </div>
                      <div class="form-group col-md-12">
                        <label class="control-label" for="">Confirm Password</label>
                        <input class="form-control" id="confirm_password" name="confirm_password"  type="password" >
                      </div>
                        </br>                    
                      <div class="form-group col-md-12">
                      <button type="submit" class="btn btn-danger">&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;</button>
                      </div>
                     
                  </form>
                 </div>
                </div>
              </div>
            </div>
        </div>
    </div>
        
    </section>
    
 @include('layouts.footer')

<script>
function validatePassword(){
	if($('#newpassword').val()==""){
		$('#newpassword').focus();
		return false;
	}else if($('#confirm_password').val()==""){
		$('#confirm_password').focus();
		return false;
	}else if($('#newpassword').val()!=$('#confirm_password').val()){
		
    alert("Password Mismatch!!!");
    $('#newpassword').val("");
    $('#confirm_password').val("");
    $('#newpassword').focus();
		return false;
	}
}
</script>