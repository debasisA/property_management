
@php

   $user=Auth::user()->type;

@endphp
<section class="side_bar" style="background:#005984;">
    <ul class="nav_menus">
           
        
     <li><a class="_active" href="#"><img src="{{asset('images/nav_home.png')}}" alt=""/>Dashboard</a></li>

          @if($user  ===  1)
            <ul>
                <li><a href="{{ url('admin/userrole/') }}" >Manage Role </a></li>
                <li><a href="{{ url('admin/users') }}" >Manage Users</a></li>
                <li><a href="{{ url('admin/checklist') }}" >Create CheckList</a></li>
                <li><a href="{{ url('admin/checkliststatus') }}" >Check List Status</a></li>
            </ul>
           @else
        	<ul >
			 <li><a href="{{ url('user-checklist') }}" >Check List</a></li>
            </ul>
          @endif
    </ul>
</section>

