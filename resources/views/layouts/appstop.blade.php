<!doctype html>
<html><head>

<meta charset="utf-8">
<title>{{ config('app.name', 'Property Management') }}</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="icon" href="images/logo.png">

<script type="text/javascript" language="javascript" src="{{ asset('js/jquery-1.11.3.min.js')}}"></script>


<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}"/>

<script src="{{asset('js/bootstrap.min.js')}}"></script>

<link type="text/css" rel="stylesheet" href="{{asset('css/default.css')}}" />
<!--<script type="text/javascript" language="javascript" src="{{asset('js/main.js')}}"></script>-->


<link rel="stylesheet" href="{{asset('css/bootstrap-select.min.css')}}"/>
<script src="{{asset('js/bootstrap-select.min.js')}}"></script>


<link rel="stylesheet" href="{{asset('fullcalendar-1.6.4/demos/cupertino/jquery-ui.min.css')}}"/>
<link href="{{asset('fullcalendar-1.6.4/fullcalendar/fullcalendar.css')}}" rel="stylesheet" />
<link href="{{asset('fullcalendar-1.6.4/fullcalendar/fullcalendar.print.css')}}" rel="stylesheet" media="print"/>
<script src="{{asset('fullcalendar-1.6.4/lib/jquery-ui.custom.min.js')}}"></script>
<script src="{{asset('fullcalendar-1.6.4/fullcalendar/fullcalendar.min.js')}}"> </script>
<script src="{{asset('chosen/chosen.jquery.js')}}" type="text/javascript"></script>
<script src="{{asset('timepicker/bootstrap-timepicker.min.js')}}"></script>


</head>

<body>