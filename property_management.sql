-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2019 at 09:24 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `property_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `checklists`
--

CREATE TABLE `checklists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `checklist_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checklist_details` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `checklist_date` date NOT NULL,
  `checklist_time` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `checklists`
--

INSERT INTO `checklists` (`id`, `role_id`, `checklist_title`, `checklist_details`, `checklist_date`, `checklist_time`, `status`, `created_at`, `updated_at`) VALUES
(4, 5, 'assassa', 'asssas', '2019-11-05', '12:00 AM', 1, '2019-11-04 22:30:17', '2019-11-04 22:32:12');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_29_044453_create_roles_table', 1),
(4, '2019_10_29_044555_create_userrole_table', 1),
(5, '2019_10_29_044702_create_checklist_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(5, 'Property Manager', '2019-10-30 23:22:08', '2019-10-30 23:22:08'),
(6, 'Property owner', '2019-10-30 23:24:21', '2019-10-30 23:24:21'),
(7, 'Renter', '2019-10-30 23:24:35', '2019-10-30 23:24:35'),
(8, 'Cleaning company', '2019-10-30 23:24:48', '2019-10-30 23:24:48');

-- --------------------------------------------------------

--
-- Table structure for table `userroles`
--

CREATE TABLE `userroles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `userroles`
--

INSERT INTO `userroles` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(10, 2, 5, NULL, NULL),
(11, 2, 8, NULL, NULL),
(37, 3, 6, NULL, NULL),
(38, 3, 7, NULL, NULL),
(39, 4, 6, NULL, NULL),
(40, 4, 7, NULL, NULL),
(41, 4, 8, NULL, NULL),
(53, 5, 6, NULL, NULL),
(54, 5, 7, NULL, NULL),
(55, 5, 8, NULL, NULL),
(62, 6, 8, NULL, NULL),
(70, 7, 5, NULL, NULL),
(71, 7, 8, NULL, NULL),
(72, 8, 6, NULL, NULL),
(73, 8, 7, NULL, NULL),
(74, 8, 8, NULL, NULL),
(77, 9, 6, NULL, NULL),
(78, 9, 7, NULL, NULL),
(79, 9, 8, NULL, NULL),
(90, 10, 6, NULL, NULL),
(91, 10, 7, NULL, NULL),
(92, 10, 8, NULL, NULL),
(97, 11, 6, NULL, NULL),
(98, 11, 7, NULL, NULL),
(99, 11, 8, NULL, NULL),
(102, 12, 5, NULL, NULL),
(103, 12, 6, NULL, NULL),
(104, 12, 7, NULL, NULL),
(105, 12, 8, NULL, NULL),
(126, 13, 6, NULL, NULL),
(127, 13, 7, NULL, NULL),
(128, 13, 8, NULL, NULL),
(129, 16, 6, NULL, NULL),
(130, 16, 7, NULL, NULL),
(131, 16, 8, NULL, NULL),
(132, 18, 5, NULL, NULL),
(133, 18, 6, NULL, NULL),
(134, 18, 8, NULL, NULL),
(135, 20, 5, NULL, NULL),
(136, 20, 6, NULL, NULL),
(137, 20, 8, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'BBSR',
  `phone` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '123456789',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL DEFAULT '2',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `phone`, `email`, `email_verified_at`, `password`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(19, 'Bobby', 'USA', '123456789', 'demo@demo.com', NULL, '$2y$10$bA9LCEPdKv6YQv45ytY1..3Y8M5yqSnSaV7rZo50oDEUM6IuRCPV2', 1, NULL, NULL, NULL),
(20, 'Debasis', 'Bhubaneswar', '9853247406', 'deb@deb.com', NULL, '$2y$10$vpa1MqiwyMxSnElBmUTIlu79Oye8n9GMtPlSNL8pB5n.K6EhPQ6uO', 2, NULL, '2019-11-05 02:52:59', '2019-11-05 02:52:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checklists`
--
ALTER TABLE `checklists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userroles`
--
ALTER TABLE `userroles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checklists`
--
ALTER TABLE `checklists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `userroles`
--
ALTER TABLE `userroles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
